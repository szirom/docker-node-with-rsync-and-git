# Node.js with rsync

## Base Docker image
* [library/node](https://hub.docker.com/r/library/node/)

Latest node.js.
Pre-installed rsync and git.

I use it for build and sync my projects to remote host.
