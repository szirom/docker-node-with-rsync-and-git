FROM node:latest

MAINTAINER szirom "szirmai.peter@gmail.com"

RUN apt-get update && \
    apt-get -y install rsync git
